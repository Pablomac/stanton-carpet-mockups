/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp'),
    jshint = require('gulp-jshint'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    styl = require('gulp-styl'),
    path = require('path'),
    lr = require('tiny-lr'), 
    server = lr(),
    refresh = require('gulp-livereload'),
    gutil = require('gulp-util'),
    open = require('gulp-open');

// define the default task and add the watch task to it
gulp.task('default', ['watch','scripts','styles','url']);

gulp.task('url', function(){
  gulp.src('index.html').pipe(open());
});

gulp.task('scripts', function() {  
    gulp.src(['src/**/*.js'])
        .pipe(gulp.dest('build'))
        .pipe(refresh(server));
});

gulp.task('styles', function() {  
    gulp.src(['css/**/*.css'])
        .pipe(styl({compress : true}))
        .pipe(gulp.dest('build'))
        .pipe(refresh(server));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('src/js/*.js', ['js']);
  gulp.watch('src/css/*.css', ['css']);
});