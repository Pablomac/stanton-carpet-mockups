module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      css: {
        src: 'src/css/*.css',
        dest: 'build/css/concat.css'
      },      
      js: {
        src: 'src/js/*.js',
        dest: 'build/js/concat.js'
      }
    },
    cssmin: {
       dist: {
          options: {
            keepSpecialComments: 0
          },
          files: [{
            expand: true,
            cwd: 'build/',
            src: ['**/*.css', '!**/_*.css'],
            dest: 'build/',
            ext: '.min.css',
        }]
      }
    },
    uglify: {
       dist: {
          files: [{
            expand: true,
            cwd: 'build/',
            src: ['**/*.js', '!**/_*.js'],
            dest: 'build/',
            ext: '.min.js',
        }]
       }
    },
    imagemin: {
      files: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: ['**/*.{png,jpg,gif,ico}', '!**/_*.{png,jpg,gif,ico}'],
          dest: 'build/',
        }]
      }
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'index.min.html': 'index.html'
        }
      }
    },
    watch:{
      js:{
        files:'src/js/*.js',
        tasks:'js'
      },
      css:{
        files:'src/css/*.css',
        tasks:'css'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default',
    ['concat',
    'cssmin',
    'uglify',
    'imagemin',
    'htmlmin']);

};